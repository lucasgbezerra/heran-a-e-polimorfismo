#include "hexagono.hpp"
#include <iostream>

Hexagono::Hexagono()
{
    set_tipo("Hexagono");
    set_altura(1.25);
    set_base(2.5);
}
Hexagono::~Hexagono()
{
    cout << "Destruindo o objeto: " << get_tipo() << endl;
}
float Hexagono::calcula_area()
{
    return (get_altura()*get_base()/2)*6;
}
float Hexagono::calcula_perimetro()
{
    return get_base()*6;
}
