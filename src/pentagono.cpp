#include "pentagono.hpp"
#include <iostream>

Pentagono::Pentagono()
{
    set_altura(2.5f);
    set_tipo("Pentagono");
    set_base(5.0f);
}

Pentagono::~Pentagono()
{
    cout << "Destruindo o objeto: " << get_tipo() << endl;
}
float Pentagono::calcula_area()
{
    return (get_base()*get_altura()/2)*5;
}
float Pentagono::calcula_perimetro()
{
    return get_base()*5;
}