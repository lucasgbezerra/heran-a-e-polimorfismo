#include "formageometrica.hpp"
#include "quadrado.hpp"
#include "triangulo.hpp"
#include "circulo.hpp"
#include "paralelogramo.hpp"
#include "pentagono.hpp"
#include "hexagono.hpp"

#include<vector>
#include <iostream>

using namespace std;

int main()
{

    vector<FormaGeometrica *> lista_de_formas;
    
    lista_de_formas.push_back(new Quadrado());
    lista_de_formas.push_back(new Triangulo());
    lista_de_formas.push_back(new Circulo());
    lista_de_formas.push_back(new Paralelogramo());
    lista_de_formas.push_back(new Pentagono());
    lista_de_formas.push_back(new Hexagono());
    
    for(FormaGeometrica * p: lista_de_formas )
    {
        cout << "Tipo: " << p->get_tipo() << endl;
        cout << "Area: " << p->calcula_area() << endl;
        cout << "Perimetro: " << p->calcula_perimetro() << endl;
    }
    
    return 0;
}