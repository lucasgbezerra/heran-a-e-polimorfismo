#include "quadrado.hpp"
#include <iostream>
 
using namespace std;

Quadrado::Quadrado()
{
    set_tipo("Quadrado");
    set_base(5.0f);
    set_altura(get_base()) ;
}
Quadrado::Quadrado(float base, float altura)
{
    set_tipo("Quadrado");
    if (altura != base)
        throw(1);
    set_base(base);
    set_altura(base);
}

Quadrado::~Quadrado()
{
    cout << "Destruindo o objeto: " << get_tipo() << endl;    
}
