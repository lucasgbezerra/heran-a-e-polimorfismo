#include "triangulo.hpp"
#include <iostream>
#include <math.h>

using namespace std;

Triangulo::Triangulo()
{
    set_tipo("Triangulo");
    set_base(5.0f);
    set_altura(5.0f);
}
Triangulo::~Triangulo()
{
    cout << "Destruindo o objeto: " << get_tipo() << endl;

}

float Triangulo::calcula_area()
{
   return get_base()*get_altura()/2;
}

float Triangulo::calcula_perimetro()
{
    return sqrt(get_base()*get_base() + get_altura()*get_altura()) + get_base()+ get_altura();
}