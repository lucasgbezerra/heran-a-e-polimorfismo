#include "circulo.hpp"
#include <iostream>
#include <math.h>
#define PI 3.14159365

using namespace std;

Circulo::Circulo()
{
    raio = 3.0f;
    set_tipo("Circulo");

}

Circulo::~Circulo()
{
    cout << "Destruindo o objeto: " << get_tipo() << endl;
}

void Circulo::set_raio(float raio)
{
    this->raio = raio;

}
float Circulo::get_raio()
{
    return this->raio;
}
float Circulo::calcula_area()
{
    return PI*pow(get_raio(),2);

}
float Circulo::calcula_perimetro()
{
    return 2*PI*get_raio();

}
