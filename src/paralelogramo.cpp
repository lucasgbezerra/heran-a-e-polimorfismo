#include "paralelogramo.hpp"
#include <iostream>

Paralelogramo::Paralelogramo()
{
    set_tipo("Paralelogramo");
    set_base(7.0f);
    set_altura(5.0f);
}
Paralelogramo::~Paralelogramo()
{
    cout << "Destruindo o objeto: " << get_tipo() << endl;
}